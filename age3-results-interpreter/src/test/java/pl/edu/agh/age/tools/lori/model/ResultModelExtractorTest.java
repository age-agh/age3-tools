/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;

public class ResultModelExtractorTest {

	static { Locale.setDefault(new Locale("pl", "PL")); }

	@Test
	void testEmptyFile() throws URISyntaxException {
		final URL resource = getClass().getResource("/emas-20180101T000000.log");
		assertThatThrownBy(() -> ResultModelExtractor.obtainResultModel(new File(resource.toURI()))).isExactlyInstanceOf(IllegalArgumentException.class);
	}

	@Test
	void testEmptyProperties() throws URISyntaxException, IOException {
		final URL resource = getClass().getResource("/emas-20180101T000001.log");
		final ResultModel resultModel = ResultModelExtractor.obtainResultModel(new File(resource.toURI()));

		assertThat(resultModel).isNotNull();
	}


	@Test
	void testFileParsing() throws URISyntaxException, IOException {
		final URL resource = getClass().getResource("/emas-20180216T144726.log");
		final ResultModel resultModel = ResultModelExtractor.obtainResultModel(new File(resource.toURI()));

		assertThat(resultModel).isNotNull();
	}
}
