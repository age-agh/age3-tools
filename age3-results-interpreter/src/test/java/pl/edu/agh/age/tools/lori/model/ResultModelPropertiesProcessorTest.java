/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model;

import static org.assertj.core.api.Assertions.assertThat;

import pl.edu.agh.age.tools.lori.model.entry.LogEntries;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ResultModelPropertiesProcessorTest {

	private LogEntries logEntries;

	@BeforeEach public void beforeEach() {
		logEntries = new LogEntries();
	}

	@Test void parseEmptyProperties() {
		final ResultModelPropertiesProcessor processor = new ResultModelPropertiesProcessor(logEntries);

		final boolean opening = processor.processLine("<properties>");
		final boolean closing = processor.processLine("</properties>");

		assertThat(opening).isTrue();
		assertThat(closing).isTrue();
		assertThat(logEntries.properties()).isEmpty();
	}

	@Test void parseNoOpeningTag() {
		final ResultModelPropertiesProcessor processor = new ResultModelPropertiesProcessor(logEntries);

		final boolean closing = processor.processLine("</properties>");

		assertThat(closing).isFalse();
		assertThat(logEntries.properties()).isEmpty();
	}


	@Test void parseNoTagsButProperty() {
		final ResultModelPropertiesProcessor processor = new ResultModelPropertiesProcessor(logEntries);

		final boolean result = processor.processLine("prop = value");

		assertThat(result).isFalse();
		assertThat(logEntries.properties()).isEmpty();
	}

	@Test void parseOneProperty() {
		final ResultModelPropertiesProcessor processor = new ResultModelPropertiesProcessor(logEntries);

		final boolean opening = processor.processLine("<properties>");
		final boolean property = processor.processLine("prop = value");
		final boolean closing = processor.processLine("</properties>");

		assertThat(opening).isTrue();
		assertThat(property).isTrue();
		assertThat(closing).isTrue();
		assertThat(logEntries.properties()).hasSize(1); // FIXME content check
	}

	@Test void parsePropertyWithoutEqualSign() {
		final ResultModelPropertiesProcessor processor = new ResultModelPropertiesProcessor(logEntries);

		final boolean opening = processor.processLine("<properties>");
		final boolean property = processor.processLine("prop without equal sign");
		final boolean closing = processor.processLine("</properties>");

		assertThat(opening).isTrue();
		assertThat(property).isTrue();
		assertThat(closing).isTrue();
		assertThat(logEntries.properties()).hasSize(1);
	}
}
