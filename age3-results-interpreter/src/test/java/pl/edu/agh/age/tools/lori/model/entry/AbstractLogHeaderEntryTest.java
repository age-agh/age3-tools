/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model.entry;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import pl.edu.agh.age.compute.stream.logging.DefaultLoggingService;

import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

@SuppressWarnings("AnonymousInnerClassMayBeStatic")
public final class AbstractLogHeaderEntryTest {

	@Test void nullProperty() {
		final LogHeaderEntry headerEntry = new LogHeaderEntry("") {};

		assertThatThrownBy(() -> headerEntry.indexOf(null)).isExactlyInstanceOf(NullPointerException.class);
	}

	@Test void emptyHeader() {
		final LogHeaderEntry headerEntry = new LogHeaderEntry("") {};

		assertThatThrownBy(() -> headerEntry.indexOf("no-such-property")).isExactlyInstanceOf(NoSuchElementException.class);
	}

	@Test void correctProperties() {
		final LogHeaderEntry headerEntry = new LogHeaderEntry("one" + DefaultLoggingService.DELIMITER + "two" + DefaultLoggingService.DELIMITER + "three") {};

		assertThat(headerEntry.indexOf("one")).isEqualTo(0);
		assertThat(headerEntry.indexOf("two")).isEqualTo(1);
		assertThat(headerEntry.indexOf("three")).isEqualTo(2);
	}
}
