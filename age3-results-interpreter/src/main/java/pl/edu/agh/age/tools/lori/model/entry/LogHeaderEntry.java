/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model.entry;

import static java.util.Objects.requireNonNull;

import com.google.common.collect.ImmutableMap;

import java.util.NoSuchElementException;

import javax.annotation.concurrent.Immutable;

/**
 * Header entries describes orders of fields in value entries.
 */
@Immutable
public class LogHeaderEntry extends LogEntry {

	private final ImmutableMap<String, Integer> indexesMap;

	LogHeaderEntry(final String logEntryValues) {
		super(logEntryValues);
		final ImmutableMap.Builder<String, Integer> builder = ImmutableMap.builder();
		for (int i = 0; i < length(); i++) {
			builder.put(values().get(i), i);
		}
		indexesMap = builder.build();
	}

	/**
	 * Returns the index of the given field
	 *
	 * @param property
	 * 		a name of the field
	 *
	 * @return an index of the field
	 *
	 * @throws NoSuchElementException if the given property cannot be found in this header.
	 */
	public final Integer indexOf(final String property) {
		requireNonNull(property);
		if (!indexesMap.containsKey(property)) {
			throw new NoSuchElementException("No such property in the header");
		}
		return indexesMap.get(property);
	}

}
