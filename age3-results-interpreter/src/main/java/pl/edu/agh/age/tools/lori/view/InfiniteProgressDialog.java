/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.view;

import pl.edu.agh.age.tools.lori.ResultsInterpreter;

import java.util.ResourceBundle;

import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public final class InfiniteProgressDialog {

	private static final int DIALOG_PREFERRED_WIDTH = 340;

	private static final double INDICATOR_SIZE = 40.0;

	private static final double PADDING = 15.0;

	private static final double SPACING = 12.0;

	private static final ResourceBundle bundle = ResourceBundle.getBundle("pl.edu.agh.age.tools.lori.strings");

	private boolean isClosingAllowed = false;

	private Label message;

	private Stage dialog;

	public InfiniteProgressDialog(final String message) {
		dialog = new Stage();
		dialog.setResizable(false);
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initStyle(StageStyle.UTILITY);
		dialog.setTitle(bundle.getString("progress.DIALOG_TITLE"));
		dialog.setScene(new Scene(createDialogPane(message)));
		dialog.setOnCloseRequest(event -> {
			// [dymara] Closing progress dialog with 'x' window button is disabled on purpose.
			if (!isClosingAllowed) {
				event.consume();
			}
		});
		final Stage applicationStage = ResultsInterpreter.stage();
		dialog.initOwner(applicationStage);
		Bindings.bindContent(dialog.getIcons(), applicationStage.getIcons());
	}

	private Parent createDialogPane(final String initialMessage) {
		message = new Label(initialMessage);
		message.setWrapText(true);
		message.setPadding(new Insets(0, PADDING / 2.0, 0, 0));

		final HBox container = new HBox();
		container.setSpacing(SPACING);
		container.setAlignment(Pos.CENTER_LEFT);
		container.setPrefWidth(DIALOG_PREFERRED_WIDTH);
		container.setPadding(new Insets(PADDING));

		final ProgressIndicator indicator = new ProgressIndicator();
		indicator.setPrefSize(INDICATOR_SIZE, INDICATOR_SIZE);

		container.getChildren().addAll(indicator, message);

		return container;
	}

	public void setMessage(final String messageText) {
		message.setText(messageText);
	}

	public void show() {
		dialog.sizeToScene();
		dialog.show();
	}

	public void hide() {
		isClosingAllowed = true;
		dialog.hide();
		dialog.setOnCloseRequest(null);
	}

}
