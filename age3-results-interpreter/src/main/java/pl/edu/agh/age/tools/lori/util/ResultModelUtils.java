/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.util;

import static com.google.common.base.Preconditions.checkArgument;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ResultModelUtils {

	private static final String DATE_REGEX = "\\d{8}T\\d{6}";

	private static final Pattern REGEX = Pattern.compile(DATE_REGEX);

	private static final String DATE_FORMATTER = "yyyyMMdd'T'HHmmss";

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMATTER);

	private ResultModelUtils() { }

	public static LocalDateTime resolveDateFromPath(final String filePath) {
		final Matcher matcher = REGEX.matcher(filePath);
		checkArgument(matcher.find(), "No date and time in file name");
		final String dateString = matcher.group(0);
		return LocalDateTime.parse(dateString, FORMATTER);
	}

}
