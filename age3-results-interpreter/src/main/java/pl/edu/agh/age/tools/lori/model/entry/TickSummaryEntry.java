/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model.entry;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.requireNonNull;

import pl.edu.agh.age.compute.stream.logging.Columns;

import javax.annotation.concurrent.Immutable;

@Immutable
public final class TickSummaryEntry extends LogEntry {

	private final double time;

	private final double bestFitnessSoFar;

	private final long fitnessEvaluations;

	public TickSummaryEntry(final TickSummaryHeaderEntry header, final String logEntryValues) {
		super(logEntryValues);
		requireNonNull(header);
		checkState(header.length() <= length());

		time = getTimeAsDouble(header.indexOf(Columns.TIME));
		bestFitnessSoFar = getAsDouble(header.indexOf(Columns.BEST_SOLUTION_SO_FAR));
		fitnessEvaluations = getAsLong(header.indexOf(Columns.FITNESS_EVALUATIONS));
	}

	public double time() {
		return time;
	}

	public double bestFitnessSoFar() {
		return bestFitnessSoFar;
	}

	public long fitnessEvaluations() {
		return fitnessEvaluations;
	}

}
