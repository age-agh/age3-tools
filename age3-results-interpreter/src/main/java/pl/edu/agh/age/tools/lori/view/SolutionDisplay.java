/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.view;

import pl.edu.agh.age.compute.labs.evaluator.LabsEnergy;
import pl.edu.agh.age.compute.labs.evaluator.LabsEvaluator;
import pl.edu.agh.age.compute.labs.solution.LabsSolution;
import pl.edu.agh.age.compute.ogr.RulerUtils;
import pl.edu.agh.age.compute.ogr.evaluator.RulerEvaluator;
import pl.edu.agh.age.compute.ogr.solution.Ruler;
import pl.edu.agh.age.compute.stream.emas.solution.Solution;
import pl.edu.agh.age.tools.lori.model.entry.BestSolutionEntry;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Pair;

/**
 * A widget that displays a single solution:
 * - one line of solution data,
 * - one line of solution properties,
 * - a summary label.
 */
@SuppressWarnings("InstanceVariableMayNotBeInitialized")
public final class SolutionDisplay extends VBox {

	private static final double TEXT_FIELD_WIDTH = 30.0;

	private static final double TEXT_FIELD_HEIGHT = 30.0;

	private static final ResourceBundle bundle = ResourceBundle.getBundle("pl.edu.agh.age.tools.lori.strings");

	@FXML private HBox textFieldsContainer;

	@FXML private HBox propertiesContainer;

	@FXML private Label summaryLabel;

	@SuppressWarnings("ThisEscapedInObjectConstruction") public SolutionDisplay(final BestSolutionEntry solutionEntry) {
		final FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("solutionDisplay.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (final IOException exception) {
			throw new UncheckedIOException("Could not load FXML file for SolutionDisplay", exception);
		}

		final List<String> solution = solutionEntry.solutionValues();
		final long stepNumber = solutionEntry.stepNumber();
		final long workplaceId = solutionEntry.workplaceId();
		final long count = solutionEntry.occurrenceCount();

		createTextFields(solution);
		createPropertyLabels(resolveProperties(solutionEntry.solution()));
		createSummary(stepNumber, workplaceId, count);
	}

	private void createTextFields(final List<String> solution) {
		final List<TextField> textFields = new ArrayList<>(solution.size());
		for (final String elem : solution) {
			final TextField field = new TextField(elem);
			field.setEditable(false);
			field.setAlignment(Pos.CENTER);
			field.setPrefHeight(TEXT_FIELD_HEIGHT);
			field.setMinWidth(TEXT_FIELD_WIDTH);
			textFields.add(field);
		}
		textFieldsContainer.getChildren().addAll(textFields);
	}

	private void createPropertyLabels(final Collection<Pair<String, String>> properties) {
		final List<Label> propertyLabels = new ArrayList<>(properties.size());
		for (final Pair<String, String> property : properties) {
			final Label propertyName = new Label(property.getKey());
			propertyLabels.add(propertyName);
			final Label propertyValue = new Label(property.getValue());
			propertyValue.setStyle("-fx-font-weight: bold");
			propertyLabels.add(propertyValue);
		}
		propertiesContainer.getChildren().addAll(propertyLabels);
	}

	private void createSummary(final long stepId, final long workplaceId, final long count) {
		summaryLabel.setText(String.format(bundle.getString("solution-display.summary"), stepId, workplaceId, count));
	}


	private static Collection<Pair<String, String>> resolveProperties(final Solution<?> solution) {
		if (solution instanceof LabsSolution) {
			final double labsEnergy = new LabsEnergy((LabsSolution)solution).value();
			final double labsMeritFactor = LabsEvaluator.meritFactorOf(((LabsSolution)solution).length(), labsEnergy);
			final String formattedEnergy = String.format("%.0f", labsEnergy);
			final String formattedMeritFactor = String.format("%.4f", labsMeritFactor);
			final Pair<String, String> energy = new Pair<>(bundle.getString("solution.LABS_ENERGY_STRING"),
			                                               formattedEnergy);
			final Pair<String, String> meritFactor = new Pair<>(bundle.getString("solution.LABS_MERIT_FACTOR_STRING"),
			                                                    formattedMeritFactor);
			return Arrays.asList(energy, meritFactor);
		} else if (solution instanceof Ruler) {
			final int violationsCount = RulerUtils.calculateViolations((Ruler)solution);
			final double rulerFitness = RulerEvaluator.evaluate(((Ruler)solution).length(), violationsCount);
			final String formattedFitness = String.format("%.0f", rulerFitness);
			final String formattedLength = Long.toString(((Ruler)solution).length());
			final String formattedViolations = Integer.toString(violationsCount);
			final Pair<String, String> fitness = new Pair<>(bundle.getString("solution.OGR_FITNESS_STRING"),
			                                                formattedFitness);
			final Pair<String, String> length = new Pair<>(bundle.getString("solution.OGR_LENGTH_STRING"),
			                                               formattedLength);
			final Pair<String, String> violations = new Pair<>(bundle.getString("solution.OGR_VIOLATIONS_STRING"),
			                                                   formattedViolations);
			return Arrays.asList(fitness, length, violations);
		}
		return Collections.emptyList();
	}
}
