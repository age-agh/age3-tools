/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.requireNonNull;
import static javafx.collections.FXCollections.observableList;
import static pl.edu.agh.age.tools.lori.util.ResultModelUtils.resolveDateFromPath;

import pl.edu.agh.age.compute.labs.problem.LabsProblem;
import pl.edu.agh.age.compute.ogr.problem.RulerProblem;
import pl.edu.agh.age.compute.stream.problem.ProblemDefinition;
import pl.edu.agh.age.tools.lori.model.entry.BestSolutionEntry;
import pl.edu.agh.age.tools.lori.model.entry.LogEntries;
import pl.edu.agh.age.tools.lori.model.entry.ProblemDefinitionEntry;
import pl.edu.agh.age.tools.lori.model.entry.TickSummaryEntry;
import pl.edu.agh.age.tools.lori.model.entry.WorkplaceEntry;
import pl.edu.agh.age.tools.lori.util.ChartSeriesHelper;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;

import one.util.streamex.StreamEx;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.util.Pair;

@SuppressWarnings("AssignmentOrReturnOfFieldWithMutableType")
@Immutable
public final class ResultModel {

	private static final String DATASET_NAME_DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATASET_NAME_DATE_FORMAT);

	private static final String DATA_SET_NAME_FORMAT = "%s – %s";

	private static final ResourceBundle bundle = ResourceBundle.getBundle("pl.edu.agh.age.tools.lori.strings");

	private final String logfilePath;

	private final String datasetName;

	private final List<Pair<String, String>> properties;

	private final @Nullable ProblemDefinitionEntry problemDefinitionEntry;

	private final ImmutableList<BestSolutionEntry> bestSolutions;

	private final List<Data<Number, Number>> bestFitnessChartData;

	private final List<Data<Number, Number>> fitnessCountChartData;

	// Per workplace data

	private final Multimap<Long, WorkplaceEntry> workplaceEntriesMap;

	private final ImmutableList<XYChart.Series<Number, Number>> stepNumberChartData;

	private final ImmutableList<XYChart.Series<Number, Number>> averageFitnessChartData;

	private final ImmutableList<XYChart.Series<Number, Number>> populationSizeChartData;

	private final ImmutableList<XYChart.Series<Number, Number>> energySumChartData;

	private final ImmutableList<XYChart.Series<Number, Number>> averageEnergyChartData;

	ResultModel(final LogEntries entries, final String logfilePath) {
		checkState(!entries.workplaceEntries().isEmpty(), "No workplace entries for the model");
		checkState(!entries.tickSummaryEntries().isEmpty(), "No summary entries for the model");

		this.logfilePath = requireNonNull(logfilePath);
		properties = entries.properties();
		problemDefinitionEntry = entries.problemDefinitionEntry();
		bestSolutions = entries.bestSolutionEntries();

		workplaceEntriesMap = prepareWorkplaceEntryMap(entries.workplaceEntries());

		datasetName = extractDataSetName(entries.problemDefinitionEntry());
		bestFitnessChartData = extractBestFitnessData(entries.tickSummaryEntries());
		fitnessCountChartData = extractFitnessCountData(entries.tickSummaryEntries());

		// Per workplace data
		stepNumberChartData = extractStepNumberData();
		averageFitnessChartData = extractAverageFitnessData();
		populationSizeChartData = extractPopulationSizeData();
		energySumChartData = extractEnergySumData();
		averageEnergyChartData = extractAverageEnergyData();
	}

	private String extractDataSetName(final ProblemDefinitionEntry entry) {
		final ProblemDefinition problemDefinition = entry.problemDefinition();

		final String problemName;
		if (problemDefinition instanceof LabsProblem) {
			problemName = "LABS";
		} else if (problemDefinition instanceof RulerProblem) {
			problemName = "OGR";
		} else {
			problemName = "<ERROR>";
		}
		return String.format(DATA_SET_NAME_FORMAT, problemName, FORMATTER.format(resolveDateFromPath(logfilePath)));
	}

	private List<Data<Number, Number>> extractBestFitnessData(final Collection<TickSummaryEntry> entries) {
		return StreamEx.of(entries).map(e -> new Data<Number, Number>(e.time(), e.bestFitnessSoFar())).toList();
	}

	private List<Data<Number, Number>> extractFitnessCountData(final Collection<TickSummaryEntry> entries) {
		return StreamEx.of(entries).map(e -> new Data<Number, Number>(e.time(), e.fitnessEvaluations())).toList();
	}


	private ImmutableList<XYChart.Series<Number, Number>> extractStepNumberData() {
		return extract("model.AVERAGE_STEP_NUMBER_SERIES", e -> new Data<>(e.time(), e.stepNumber()));
	}

	private ImmutableList<XYChart.Series<Number, Number>> extractAverageFitnessData() {
		return extract("model.AVERAGE_FITNESS_SERIES", e -> new Data<>(e.time(), e.averageFitness()));
	}

	private ImmutableList<XYChart.Series<Number, Number>> extractPopulationSizeData() {
		return extract("model.POPULATION_SUM_SERIES", e -> new Data<>(e.time(), e.populationSize()));
	}

	private ImmutableList<XYChart.Series<Number, Number>> extractEnergySumData() {
		return extract("model.POPULATION_ENERGY_SUM_SERIES", e -> new Data<>(e.time(), e.populationEnergySum()));
	}

	private ImmutableList<XYChart.Series<Number, Number>> extractAverageEnergyData() {
		return extract("model.AVERAGE_AGENT_ENERGY_SERIES", e -> new Data<>(e.time(), e.averageEnergy()));
	}

	private ImmutableList<XYChart.Series<Number, Number>> extract(final String averageSeriesName,
	                                                              final Function<WorkplaceEntry, Data<Number, Number>> mapper) {
		final List<XYChart.Series<Number, Number>> list = createWorkplacePropertyData(mapper);
		if (workplaceEntriesMap.size() > 1) {
			list.add(new XYChart.Series<>(bundle.getString(averageSeriesName),
			                              observableList(ChartSeriesHelper.generateAverageSeries(list))));
		}
		return ImmutableList.copyOf(list);
	}

	private Multimap<Long, WorkplaceEntry> prepareWorkplaceEntryMap(final Iterable<WorkplaceEntry> entries) {
		final Multimap<Long, WorkplaceEntry> map = LinkedListMultimap.create();
		entries.forEach(entry -> map.put(entry.workplaceId(), entry));
		return map;
	}

	private List<XYChart.Series<Number, Number>> createWorkplacePropertyData(
			final Function<WorkplaceEntry, Data<Number, Number>> entryFunction) {
		final List<XYChart.Series<Number, Number>> list = new ArrayList<>(workplaceEntriesMap.size());
		for (final Long workplaceId : workplaceEntriesMap.keySet()) {
			final List<Data<Number, Number>> points = workplaceEntriesMap.get(workplaceId)
			                                                             .stream()
			                                                             .map(entryFunction)
			                                                             .collect(Collectors.toList());
			final String name = String.format(bundle.getString("model.workplace"), workplaceId);
			list.add(new XYChart.Series<>(name, observableList(points)));
		}
		return list;
	}

	/* API METHODS */

	public String logfilePath() {
		return logfilePath;
	}

	public String datasetName() {
		return datasetName;
	}

	public ImmutableList<Pair<String, String>> properties() {
		return ImmutableList.copyOf(properties);
	}

	public ProblemDefinitionEntry problemDefinitionEntry() {
		return problemDefinitionEntry;
	}

	public ImmutableList<XYChart.Series<Number, Number>> stepNumberChartData() {
		return stepNumberChartData;
	}

	public ImmutableList<XYChart.Series<Number, Number>> bestFitnessChartData() {
		return ImmutableList.of(new XYChart.Series<>(bundle.getString("model.BEST_FITNESS_SERIES"),
		                                             observableList(bestFitnessChartData)));
	}

	public ImmutableList<XYChart.Series<Number, Number>> fitnessCountChartData() {
		return ImmutableList.of(new XYChart.Series<>(bundle.getString("model.FITNESS_EVALUATIONS_SERIES"),
		                                             observableList(fitnessCountChartData)));
	}

	public ImmutableList<XYChart.Series<Number, Number>> averageFitnessChartData() {
		return averageFitnessChartData;
	}

	public ImmutableList<XYChart.Series<Number, Number>> populationSizeChartData() {
		return populationSizeChartData;
	}

	public ImmutableList<XYChart.Series<Number, Number>> energySumChartData() {
		return energySumChartData;
	}

	public ImmutableList<XYChart.Series<Number, Number>> averageEnergyChartData() {
		return averageEnergyChartData;
	}

	public ImmutableList<BestSolutionEntry> bestSolutions() {
		return bestSolutions;
	}

}
