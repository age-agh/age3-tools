/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori;

import pl.edu.agh.age.tools.lori.model.ResultModel;
import pl.edu.agh.age.tools.lori.model.ResultModelExtractor;
import pl.edu.agh.age.tools.lori.model.entry.BestSolutionEntry;
import pl.edu.agh.age.tools.lori.model.entry.ProblemDefinitionEntry;
import pl.edu.agh.age.tools.lori.view.InfiniteProgressDialog;

import one.util.streamex.StreamEx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.util.Pair;

@SuppressWarnings("InstanceVariableMayNotBeInitialized")
public final class ResultsInterpreterController implements Initializable {

	private static final Logger log = LoggerFactory.getLogger(ResultsInterpreterController.class);

	private static final ResourceBundle bundle = ResourceBundle.getBundle("pl.edu.agh.age.tools.lori.strings");

	@FXML private HeaderController headerController;

	@FXML private StatisticsTabController statisticsTabController;

	@FXML private SolutionsTabController solutionsTabController;

	private final InfiniteProgressDialog progressDialog = new InfiniteProgressDialog(
			bundle.getString("controller.PROGRESS_DIALOG_MESSAGE"));

	@Override public void initialize(final URL location, final ResourceBundle resources) {
		headerController.setParentController(this);
	}

	public void lookForNewestDataFile(final String directoryPath) {
		final File directory = new File(directoryPath);
		if (directory.exists() && directory.isDirectory()) {
			final File[] logFiles = directory.listFiles(pathname -> pathname.isFile() && pathname.getName()
			                                                                                     .matches(
					                                                                                     ResultModelExtractor.LOG_FILENAME_PATTERN));
			if (logFiles.length > 0) {
				final List<File> matchedFiles = StreamEx.of(logFiles)
				                                        .sorted((f1, f2) -> f2.getName().compareTo(f1.getName()))
				                                        .toList();
				final File newestFile = matchedFiles.iterator().next();
				loadData(newestFile);
			}
		}
	}

	public void loadData(final File datasetFile) {
		log.info("Loading data from {}", datasetFile);

		final Task<ResultModel> task = new Task<ResultModel>() {
			@Override public ResultModel call() throws IOException {
				return ResultModelExtractor.obtainResultModel(datasetFile);
			}

			@Override protected void scheduled() {
				super.scheduled();
				progressDialog.show();
			}

			@Override protected void succeeded() {
				super.succeeded();
				log.info("Successfully loaded data from {}", datasetFile);
				reloadViews(getValue());
			}

			@Override protected void failed() {
				super.failed();
				log.error("Exception when loading data from {}", datasetFile, getException());
				cleanViews();
			}
		};

		final Thread thread = new Thread(task);
		thread.setDaemon(true);
		thread.start();
	}

	private void reloadViews(final ResultModel model) {
		assert model != null : "Execution path led to null model";

		final String dataSetName = model.datasetName();
		final String logFilePath = model.logfilePath();
		final ProblemDefinitionEntry problemDefinition = model.problemDefinitionEntry();
		final List<Pair<String, String>> properties = model.properties();
		final List<BestSolutionEntry> bestSolutions = model.bestSolutions();

		ResultsInterpreter.updateStageTitle(dataSetName);
		headerController.setLoadedConfigPath(logFilePath);
		headerController.setProblemParameters(problemDefinition, dataSetName, properties);
		statisticsTabController.replaceResultModel(model);
		solutionsTabController.setSolutions(bestSolutions);
		progressDialog.hide();
	}

	private void cleanViews() {
		ResultsInterpreter.resetTitle();
		progressDialog.hide();
	}

}
