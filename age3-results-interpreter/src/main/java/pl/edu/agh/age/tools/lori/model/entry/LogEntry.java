/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model.entry;

import pl.edu.agh.age.compute.stream.logging.DefaultLoggingService;

import com.google.common.collect.ImmutableList;

import java.util.List;

import javax.annotation.concurrent.Immutable;

/**
 * Basic log entry.
 *
 * A log entry is just a line with fields separated by a delimiter.
 * This class splits them and converts them to the list of strings.
 */
@Immutable
public class LogEntry {

	private final ImmutableList<String> values;

	LogEntry(final String logEntryValues) {
		values = ImmutableList.copyOf(logEntryValues.split(DefaultLoggingService.DELIMITER));
	}

	public final List<String> values() {
		return values;
	}

	public final String get(final int i) {
		return values.get(i);
	}

	public final double getAsDouble(final int i) {
		return Double.parseDouble(values.get(i));
	}

	public final long getAsLong(final int i) {
		return Long.parseLong(values.get(i));
	}

	public final int getAsInt(final int i) {
		return Integer.parseInt(values.get(i));
	}

	public final double getTimeAsDouble(final int i) {
		return getAsLong(i) / 1.0e9d;
	}

	public final int length() {
		return values.size();
	}

}
