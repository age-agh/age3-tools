/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.geometry.Point2D;

public final class SeriesReducer {

	private static final double DEFAULT_EPSILON_VALUE = 0.01;

	private SeriesReducer() { }

	/**
	 * Reduces number of points in given series using Ramer-Douglas-Peucker algorithm and a default epsilon value of
	 * 0.01;
	 *
	 * @param points
	 * 		initial, ordered list of points
	 */
	public static List<Point2D> reduce(final List<Point2D> points) {
		return reduce(points, DEFAULT_EPSILON_VALUE);
	}

	/**
	 * Reduces number of points in given series using Ramer-Douglas-Peucker algorithm.
	 *
	 * @param points
	 * 		initial, ordered list of points
	 * @param epsilon
	 * 		allowed margin of the resulting curve, has to be > 0
	 */
	private static List<Point2D> reduce(final List<Point2D> points, final double epsilon) {
		if (epsilon < 0) {
			throw new IllegalArgumentException("Epsilon cannot be less then 0.");
		}
		double furthestPointDistance = 0.0;
		int furthestPointIndex = 0;
		final PointLine line = new PointLine(points.get(0), points.get(points.size() - 1));
		for (int i = 1; i < points.size() - 1; i++) {
			final double distance = line.distance(points.get(i));
			if (distance > furthestPointDistance) {
				furthestPointDistance = distance;
				furthestPointIndex = i;
			}
		}
		if (furthestPointDistance > epsilon) {
			final List<Point2D> reduced1 = reduce(points.subList(0, furthestPointIndex + 1), epsilon);
			final List<Point2D> reduced2 = reduce(points.subList(furthestPointIndex, points.size()), epsilon);
			final List<Point2D> result = new ArrayList<>(reduced1);
			result.addAll(reduced2.subList(1, reduced2.size()));
			return result;
		} else {
			return line.asList();
		}
	}

	private static final class PointLine {

		private final Point2D start;

		private final Point2D end;

		private final double dx;

		private final double dy;

		private final double sxey;

		private final double exsy;

		private final double length;

		PointLine(final Point2D start, final Point2D end) {
			this.start = start;
			this.end = end;
			dx = start.getX() - end.getX();
			dy = start.getY() - end.getY();
			sxey = start.getX() * end.getY();
			exsy = end.getX() * start.getY();
			length = Math.sqrt(dx * dx + dy * dy);
		}

		List<Point2D> asList() {
			return Arrays.asList(start, end);
		}

		double distance(final Point2D p) {
			return Math.abs(dy * p.getX() - dx * p.getY() + sxey - exsy) / length;
		}
	}

}
