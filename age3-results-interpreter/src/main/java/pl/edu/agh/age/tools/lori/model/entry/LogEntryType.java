/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model.entry;

import static java.util.Objects.requireNonNull;

import pl.edu.agh.age.compute.stream.logging.Tags;

public enum LogEntryType {

	PROBLEM_DESCRIPTION(Tags.PROBLEM_DESCRIPTION_TAG),
	WORKPLACE_ENTRY_HEADER(Tags.WORKPLACE_ENTRY_HEADER_TAG),
	WORKPLACE_ENTRY(Tags.WORKPLACE_ENTRY_TAG),
	TICK_SUMMARY_HEADER(Tags.TICK_SUMMARY_HEADER_TAG),
	TICK_SUMMARY(Tags.TICK_SUMMARY_TAG),
	BEST_SOLUTION_HEADER(Tags.BEST_SOLUTION_HEADER_TAG),
	BEST_SOLUTION(Tags.BEST_SOLUTION_TAG),
	UNKNOWN("");

	public final String tag;

	LogEntryType(final String tag) {
		this.tag = tag;
	}

	public static LogEntryType resolveFromTag(final String tag) {
		switch (requireNonNull(tag)) {
			case Tags.PROBLEM_DESCRIPTION_TAG:
				return PROBLEM_DESCRIPTION;
			case Tags.WORKPLACE_ENTRY_HEADER_TAG:
				return WORKPLACE_ENTRY_HEADER;
			case Tags.WORKPLACE_ENTRY_TAG:
				return WORKPLACE_ENTRY;
			case Tags.TICK_SUMMARY_HEADER_TAG:
				return TICK_SUMMARY_HEADER;
			case Tags.TICK_SUMMARY_TAG:
				return TICK_SUMMARY;
			case Tags.BEST_SOLUTION_HEADER_TAG:
				return BEST_SOLUTION_HEADER;
			case Tags.BEST_SOLUTION_TAG:
				return BEST_SOLUTION;
			default:
				return UNKNOWN;
		}
	}

}
