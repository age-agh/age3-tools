/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori;

import static java.util.Objects.requireNonNull;

import pl.edu.agh.age.tools.lori.model.entry.BestSolutionEntry;
import pl.edu.agh.age.tools.lori.view.SolutionDisplay;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public final class SolutionsTabController {

	private static final Logger log = LoggerFactory.getLogger(SolutionsTabController.class);

	private static final ResourceBundle bundle = ResourceBundle.getBundle("pl.edu.agh.age.tools.lori.strings");

	@FXML private ScrollPane solutionsTab;

	@FXML private VBox container;

	@FXML private BorderPane promptPane;

	@FXML private Label emptyLabel;

	private final List<SolutionDisplay> solutions = new ArrayList<>();

	public void setSolutions(final List<BestSolutionEntry> solutionEntries) {
		requireNonNull(solutionEntries);

		container.getChildren().removeAll(solutions);
		solutions.clear();
		for (final BestSolutionEntry solution : solutionEntries) {
			solutions.add(new SolutionDisplay(solution));
		}
		log.debug("Displaying solutions: `{}`", solutions);
		container.getChildren().addAll(solutions);
		solutionsTab.setFitToHeight(false);
		promptPane.setManaged(false);
		promptPane.setVisible(false);
		container.setManaged(true);
		container.setVisible(true);
	}

	public void clearSolutions() {
		emptyLabel.setText(bundle.getString("solution.PROMPT_TEXT"));
		solutionsTab.setFitToHeight(true);
		promptPane.setManaged(true);
		promptPane.setVisible(true);
		container.setManaged(false);
		container.setVisible(false);
		container.getChildren().removeAll(solutions);
		solutions.clear();
	}

}
