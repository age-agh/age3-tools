/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.util;

import one.util.streamex.StreamEx;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;

public final class ChartSeriesHelper {

	private ChartSeriesHelper() {}

	public static List<Data<Number, Number>> generateAverageSeries(
			final List<XYChart.Series<Number, Number>> otherSeries) {

		final List<ObservableList<Data<Number, Number>>> seriesAsLists = StreamEx.of(otherSeries)
		                                                                         .map(XYChart.Series::getData)
		                                                                         .toList();

		final List<Data<Number, Number>> sampleSeries = StreamEx.of(seriesAsLists)
		                                                        .max(Comparator.comparingInt(List::size))
		                                                        .orElseThrow(() -> new IllegalArgumentException(
				                                                        "No series in data"));
		final List<Data<Number, Number>> averageSeries = new ArrayList<>();

		for (final Data<Number, Number> data : sampleSeries) {
			final Number x = data.getXValue();
			int seriesCount = 0;
			double sum = 0;
			for (final ObservableList<Data<Number, Number>> series : seriesAsLists) {
				final Optional<Data<Number, Number>> val = series.stream()
				                                                 .filter(d -> d.getXValue().equals(x))
				                                                 .findFirst();
				if (val.isPresent()) {
					sum += val.get().getYValue().doubleValue();
					seriesCount++;
				}
			}

			final double y = sum / seriesCount;
			averageSeries.add(new Data<>(x, y));
		}
		return averageSeries;
	}

	public static List<Data<Number, Number>> generateSumSeries(final List<XYChart.Series<Number, Number>> otherSeries) {

		final List<ObservableList<Data<Number, Number>>> seriesAsLists = StreamEx.of(otherSeries)
		                                                                         .map(XYChart.Series::getData)
		                                                                         .toList();

		final List<Data<Number, Number>> sampleSeries = StreamEx.of(seriesAsLists)
		                                                        .max(Comparator.comparingInt(List::size))
		                                                        .orElseThrow(() -> new IllegalArgumentException(
				                                                        "No series in data"));
		final List<Data<Number, Number>> sumSeries = new ArrayList<>();

		for (final Data<Number, Number> data : sampleSeries) {
			final Number x = data.getXValue();
			double sum = 0;
			for (final ObservableList<Data<Number, Number>> series : seriesAsLists) {
				final Optional<Data<Number, Number>> val = series.stream()
				                                                 .filter(d -> d.getXValue().equals(x))
				                                                 .findFirst();
				if (val.isPresent()) {
					sum += val.get().getYValue().doubleValue();
				}
			}
			final double y = sum;
			sumSeries.add(new Data<>(x, y));
		}
		return sumSeries;
	}

}
