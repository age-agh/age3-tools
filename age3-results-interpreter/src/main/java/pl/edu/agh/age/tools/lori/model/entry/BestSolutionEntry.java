/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model.entry;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.requireNonNull;

import pl.edu.agh.age.compute.stream.emas.solution.Solution;
import pl.edu.agh.age.compute.stream.logging.Columns;

import com.google.common.collect.ImmutableList;

import java.util.List;

import javax.annotation.concurrent.Immutable;

@Immutable
public abstract class BestSolutionEntry extends LogEntry {

	private final ImmutableList<String> solutionValues;

	private final Solution<?> solution;

	private final long workplaceId;

	private final long stepNumber;

	private final long occurrenceCount;

	BestSolutionEntry(final ProblemDefinitionEntry problem, final BestSolutionHeaderEntry header,
	                  final String logEntryValues) {
		super(logEntryValues);
		requireNonNull(problem);
		requireNonNull(header);
		checkState(header.length() <= length());

		solutionValues = extractSolutionValues(header);
		solution = resolveSolution(problem, solutionValues);
		workplaceId = getAsLong(header.indexOf(Columns.SOLUTION_WORKPLACE_ID));
		stepNumber = getAsLong(header.indexOf(Columns.SOLUTION_WORKPLACE_STEP_NUMBER));
		occurrenceCount = getAsLong(header.indexOf(Columns.SOLUTION_OCURRANCE_COUNT));
	}

	public final List<String> solutionValues() {
		return solutionValues;
	}

	public final Solution<?> solution() {
		return solution;
	}

	public final long workplaceId() {
		return workplaceId;
	}

	public final long stepNumber() {
		return stepNumber;
	}

	public final long occurrenceCount() {
		return occurrenceCount;
	}

	protected abstract Solution<?> resolveSolution(final ProblemDefinitionEntry problem,
	                                               final List<String> solutionValues);

	private ImmutableList<String> extractSolutionValues(final BestSolutionHeaderEntry header) {
		final String solutionString = get(header.indexOf(Columns.SOLUTION_STRING));
		return processSolution(solutionString);
	}

	private static ImmutableList<String> processSolution(final String rawSolution) {
		final String solutionValues = rawSolution.replaceAll("\\[\\s*|\\s*\\]", "");
		return ImmutableList.copyOf(solutionValues.split(" "));
	}

}
