/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model.entry;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import pl.edu.agh.age.compute.ogr.problem.RulerProblem;
import pl.edu.agh.age.compute.stream.problem.ProblemDefinition;

import java.util.Objects;

import javax.annotation.concurrent.Immutable;

/**
 * Entry for OGR looks like this:
 *
 * ```
 * [PD];OGR;marksCount;maxAllowedDistance;format
 * ```
 *
 * @see RulerProblem
 */
@Immutable
public final class OgrProblemDefinitionEntry extends ProblemDefinitionEntry {

	public OgrProblemDefinitionEntry(final String logEntryValues) {
		super(logEntryValues);

		checkArgument(Objects.equals("OGR", get(0)), "This is not a OGR problem");
		checkState(length() == 4, "Wrong description length for OGR");
	}

	@Override
	public OgrBestSolutionEntry createBestSolutionEntry(final BestSolutionHeaderEntry header,
	                                                    final String logEntryValues) {
		return new OgrBestSolutionEntry(this, header, logEntryValues);
	}

	@Override protected ProblemDefinition resolveProblemDefinition() {
		return new RulerProblem(getAsInt(1), getAsInt(2));
	}

	@Override protected String resolveSolutionDisplayFormat() {
		return get(3);
	}
}
