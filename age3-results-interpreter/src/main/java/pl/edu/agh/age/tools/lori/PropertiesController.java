/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Pair;

/**
 * Controller for the Properties window.
 *
 * It displays the properties key-value table.
 */
public final class PropertiesController implements Initializable {

	@FXML private TableColumn<Pair<String, String>, String> nameColumn;

	@FXML private TableColumn<Pair<String, String>, String> valueColumn;

	@FXML private TableView<Pair<String, String>> propertiesTable;

	@Override public void initialize(final URL location, final ResourceBundle resources) {
		nameColumn.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getKey()));
		valueColumn.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getValue()));
	}

	public void updateProperties(final List<Pair<String, String>> properties) {
		propertiesTable.setItems(FXCollections.observableList(properties));
	}

}
