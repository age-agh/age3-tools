/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model.entry;

import static com.google.common.base.Preconditions.checkArgument;

import pl.edu.agh.age.compute.labs.evaluator.LabsEvaluator;
import pl.edu.agh.age.compute.labs.problem.LabsProblem;
import pl.edu.agh.age.compute.labs.solution.LabsSolution;
import pl.edu.agh.age.compute.stream.emas.solution.Solution;
import pl.edu.agh.age.compute.stream.problem.EvaluatorCounter;
import pl.edu.agh.age.compute.stream.problem.ProblemDefinition;

import java.util.List;
import java.util.Objects;

public final class LabsBestSolutionEntry extends BestSolutionEntry {

	LabsBestSolutionEntry(final ProblemDefinitionEntry problem, final BestSolutionHeaderEntry header,
	                      final String logEntryValues) {
		super(problem, header, logEntryValues);
	}

	@Override
	protected Solution<?> resolveSolution(final ProblemDefinitionEntry problem, final List<String> solutionValues) {
		final ProblemDefinition problemDefinition = problem.problemDefinition();
		checkArgument(problemDefinition instanceof LabsProblem, "Passed problem is not a LABS problem");

		final LabsSolution labsSolution = resolveLabsSolution(problem.solutionDisplayFormat(), solutionValues);
		final LabsEvaluator evaluator = new LabsEvaluator(EvaluatorCounter.empty());
		return labsSolution.withFitness(evaluator.evaluate(labsSolution));
	}


	private static LabsSolution resolveLabsSolution(final String outputSolutionFormat, final List<String> values) {
		if (Objects.equals(outputSolutionFormat, LabsProblem.RUN_LENGTH_DISPLAY_FORMAT)) {
			final boolean firstSign = Objects.equals(values.get(0), "+");
			final int[] integerValues = values.subList(1, values.size()).stream().mapToInt(Integer::parseInt).toArray();
			return new LabsSolution(integerValues, firstSign);
		}
		if (Objects.equals(outputSolutionFormat, LabsProblem.INTEGER_DISPLAY_FORMAT)) {
			final int[] integerValues = values.stream().mapToInt(Integer::parseInt).toArray();
			return new LabsSolution(integerValues);
		}
		throw new IllegalArgumentException(
				String.format("Passed output solution format is not supported: %s", outputSolutionFormat));
	}
}
