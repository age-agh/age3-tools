/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model.entry;

import static com.google.common.base.Preconditions.checkArgument;

import pl.edu.agh.age.compute.ogr.evaluator.RulerEvaluator;
import pl.edu.agh.age.compute.ogr.problem.RulerProblem;
import pl.edu.agh.age.compute.ogr.solution.Ruler;
import pl.edu.agh.age.compute.stream.emas.solution.Solution;
import pl.edu.agh.age.compute.stream.problem.EvaluatorCounter;
import pl.edu.agh.age.compute.stream.problem.ProblemDefinition;

import java.util.List;
import java.util.Objects;

public final class OgrBestSolutionEntry extends BestSolutionEntry {

	OgrBestSolutionEntry(final ProblemDefinitionEntry problem, final BestSolutionHeaderEntry header,
	                     final String logEntryValues) {
		super(problem, header, logEntryValues);
	}

	@Override
	protected Solution<?> resolveSolution(final ProblemDefinitionEntry problem, final List<String> solutionValues) {
		final ProblemDefinition problemDefinition = problem.problemDefinition();
		checkArgument(problemDefinition instanceof RulerProblem, "Passed problem is not an OGR problem");

		final Ruler rulerSolution = resolveRulerSolution(problem.solutionDisplayFormat(), solutionValues);
		final RulerEvaluator evaluator = new RulerEvaluator(EvaluatorCounter.empty());
		return rulerSolution.withFitness(evaluator.evaluate(rulerSolution));
	}


	private static Ruler resolveRulerSolution(final String outputSolutionFormat, final List<String> values) {
		final int[] integerValues = values.stream().mapToInt(Integer::parseInt).toArray();
		if (Objects.equals(outputSolutionFormat, RulerProblem.DIRECT_REPRESENTATION_FORMAT)) {
			return new Ruler(integerValues, true);
		}
		if (Objects.equals(outputSolutionFormat, RulerProblem.INDIRECT_REPRESENTATION_FORMAT)) {
			return new Ruler(integerValues, false);
		}
		throw new IllegalArgumentException(
				String.format("Passed output solution format is not supported: %s", outputSolutionFormat));
	}
}
