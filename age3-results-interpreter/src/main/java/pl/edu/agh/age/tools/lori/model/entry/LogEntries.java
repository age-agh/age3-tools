/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model.entry;

import static java.util.Objects.requireNonNull;

import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import javafx.util.Pair;

@SuppressWarnings({"InstanceVariableMayNotBeInitialized", "CollectionWithoutInitialCapacity"})
public final class LogEntries {

	private final List<Pair<String, String>> properties = new ArrayList<>();

	private @Nullable ProblemDefinitionEntry problemDefinitionEntry;

	private @Nullable WorkplaceHeaderEntry workplaceHeader;

	private final List<WorkplaceEntry> workplaceEntries = new ArrayList<>();

	private @Nullable TickSummaryHeaderEntry tickSummaryHeader;

	private final List<TickSummaryEntry> summaryEntries = new ArrayList<>();

	private @Nullable BestSolutionHeaderEntry bestSolutionHeader;

	private final List<BestSolutionEntry> bestSolutionEntries = new ArrayList<>();

	public List<Pair<String, String>> properties() {
		return ImmutableList.copyOf(properties);
	}

	public void addProperty(final Pair<String, String> property) {
		properties.add(requireNonNull(property));
	}

	public @Nullable ProblemDefinitionEntry problemDefinitionEntry() {
		return problemDefinitionEntry;
	}

	public void setProblemDefinition(final ProblemDefinitionEntry entry) {
		problemDefinitionEntry = requireNonNull(entry);
	}

	public @Nullable WorkplaceHeaderEntry workplaceHeader() {
		return workplaceHeader;
	}

	public void setWorkplaceHeader(final WorkplaceHeaderEntry entry) {
		workplaceHeader = requireNonNull(entry);
	}

	public List<WorkplaceEntry> workplaceEntries() {
		return ImmutableList.copyOf(workplaceEntries);
	}

	public void addWorkplaceEntry(final WorkplaceEntry entry) {
		workplaceEntries.add(entry);
	}

	public @Nullable TickSummaryHeaderEntry tickSummaryHeader() {
		return tickSummaryHeader;
	}

	public void setTickSummaryHeader(final TickSummaryHeaderEntry entry) {
		tickSummaryHeader = requireNonNull(entry);
	}

	public List<TickSummaryEntry> tickSummaryEntries() {
		return ImmutableList.copyOf(summaryEntries);
	}

	public void addTickSummaryEntry(final TickSummaryEntry entry) {
		summaryEntries.add(requireNonNull(entry));
	}

	public @Nullable BestSolutionHeaderEntry bestSolutionHeader() {
		return bestSolutionHeader;
	}

	public void setBestSolutionHeader(final BestSolutionHeaderEntry entry) {
		bestSolutionHeader = requireNonNull(entry);
	}

	public ImmutableList<BestSolutionEntry> bestSolutionEntries() {
		return ImmutableList.copyOf(bestSolutionEntries);
	}

	public void addBestSolutionEntry(final BestSolutionEntry entry) {
		bestSolutionEntries.add(requireNonNull(entry));
	}

}
