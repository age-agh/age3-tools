/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori;

import pl.edu.agh.age.compute.labs.problem.LabsProblem;
import pl.edu.agh.age.compute.ogr.problem.RulerProblem;
import pl.edu.agh.age.compute.stream.problem.ProblemDefinition;
import pl.edu.agh.age.tools.lori.model.entry.ProblemDefinitionEntry;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.Nullable;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.util.Pair;

@SuppressWarnings("InstanceVariableMayNotBeInitialized")
public final class HeaderController implements Initializable {

	private final ResourceBundle bundle = ResourceBundle.getBundle("pl.edu.agh.age.tools.lori.strings");

	private @Nullable File recentlyUsedDir;

	private Dialog<ButtonType> configurationModal;

	private ResultsInterpreterController parentController;

	private PropertiesController propertiesController;

	@FXML private Label loadedConfigLabel;

	@FXML private Label loadedConfigPath;

	@FXML private HBox problemDefinitionContainer;

	@FXML private Button showPropertiesButton;

	@FXML private TextFlow configurationDescription;

	/**
	 * @throws UncheckedIOException
	 * 		if the configuration modal definition cannot be loaded.
	 */
	@Override public void initialize(final URL location, final ResourceBundle resources) {
		try {
			configurationModal = createConfigurationModal();
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot create configuration modal", e);
		}
	}

	@FXML private void openButtonPressed(final ActionEvent event) {
		final FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(bundle.getString("header.FILE_CHOOSER_TITLE"));
		fileChooser.setInitialDirectory(recentlyUsedDir);
		fileChooser.getExtensionFilters()
		           .add(new FileChooser.ExtensionFilter(bundle.getString("header.LOG_EXTENSION_DESC"), "*.log"));
		final File file = fileChooser.showOpenDialog(ResultsInterpreter.stage());
		if (file != null) {
			recentlyUsedDir = new File(file.getParent());
			parentController.loadData(file);
		}
	}

	@FXML private void showPropertiesButtonPressed(final ActionEvent event) {
		closeConfigurationModal();
		configurationModal.show();
	}


	private void closeConfigurationModal() {
		if (configurationModal.isShowing()) {
			configurationModal.hide();
		}
	}

	private Dialog<ButtonType> createConfigurationModal() throws IOException {
		final FXMLLoader loader = new FXMLLoader(getClass().getResource("properties.fxml"), bundle);
		final AnchorPane anchorPane = loader.load();
		propertiesController = loader.getController();
		final Dialog<ButtonType> dialog = new Dialog<>();
		dialog.initModality(Modality.NONE);
		dialog.setResizable(true);
		dialog.getDialogPane().setContent(anchorPane);
		dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
		Platform.runLater(() -> dialog.initOwner(ResultsInterpreter.stage()));
		return dialog;
	}

	public void setParentController(final ResultsInterpreterController parentController) {
		this.parentController = parentController;
	}


	public void setLoadedConfigPath(final String path) {
		closeConfigurationModal();
		if (path != null) {
			loadedConfigLabel.setVisible(true);
			loadedConfigLabel.setManaged(true);
			loadedConfigPath.setText(path);
		} else {
			loadedConfigLabel.setVisible(false);
			loadedConfigLabel.setManaged(false);
			loadedConfigPath.setText(bundle.getString("header.noResults"));
		}
	}

	public void setProblemParameters(final @Nullable ProblemDefinitionEntry problemDefinitionEntry,
	                                 final String datasetName, final List<Pair<String, String>> properties) {
		closeConfigurationModal();
		problemDefinitionContainer.setVisible(problemDefinitionEntry != null);
		problemDefinitionContainer.setManaged(problemDefinitionEntry != null);
		configurationDescription.getChildren().removeIf(node -> true);

		final ProblemDefinition problem = (problemDefinitionEntry != null)
		                                  ? problemDefinitionEntry.problemDefinition()
		                                  : null;
		if (problem instanceof LabsProblem) {
			setLabsProblemDefinition(problemDefinitionEntry);
		} else if (problem instanceof RulerProblem) {
			setRulerProbemDefinition(problemDefinitionEntry);
		} else {
			throw new AssertionError("Problem is unknown");
		}
		HBox.setHgrow(configurationDescription, Priority.ALWAYS);
		processProperties(datasetName, properties);
	}

	private void setLabsProblemDefinition(final ProblemDefinitionEntry problemDefinitionEntry) {
		final LabsProblem problem = (LabsProblem)problemDefinitionEntry.problemDefinition();

		final Text text1 = new Text(bundle.getString("header.configurationString1"));
		final Text text2 = new Text(". " + bundle.getString("header.configurationString2"));
		final Text text3 = new Text(". " + bundle.getString("header.configurationString3"));
		final Text text4 = new Text(".");

		final Text name = new Text(bundle.getString("header.labsName"));
		name.setStyle("-fx-font-weight: bold");

		final Text params = new Text(String.format(bundle.getString("header.labsParams"), problem.problemSize()));
		params.setStyle("-fx-font-weight: bold");

		final Text format = new Text(problemDefinitionEntry.solutionDisplayFormat());
		format.setStyle("-fx-font-weight: bold");

		configurationDescription.getChildren().addAll(text1, name, text2, params, text3, format, text4);
	}

	private void setRulerProbemDefinition(final ProblemDefinitionEntry problemDefinitionEntry) {
		final RulerProblem problem = (RulerProblem)problemDefinitionEntry.problemDefinition();

		final Text text1 = new Text(bundle.getString("header.configurationString1"));
		final Text text2 = new Text(". " + bundle.getString("header.configurationString2"));
		final Text text3 = new Text(". " + bundle.getString("header.configurationString3") + '.');

		final Text name = new Text(bundle.getString("header.ogrName"));
		name.setStyle("-fx-font-weight: bold");

		final Text params = new Text(String.format(bundle.getString("header.ogrParams"), problem.marksCount()));
		params.setStyle("-fx-font-weight: bold");

		final Text format = new Text(problemDefinitionEntry.solutionDisplayFormat());
		format.setStyle("-fx-font-weight: bold");

		configurationDescription.getChildren().addAll(text1, name, text2, params, text3, format);
	}


	private void processProperties(final String datasetName, final List<Pair<String, String>> properties) {
		assert properties != null : "Passed properties are null";
		if (!properties.isEmpty()) {
			showPropertiesButton.setVisible(true);
			propertiesController.updateProperties(properties);
			configurationModal.setTitle(String.format(bundle.getString("header.PROPERTIES_WINDOW_TITLE"), datasetName));
		}
	}

}
