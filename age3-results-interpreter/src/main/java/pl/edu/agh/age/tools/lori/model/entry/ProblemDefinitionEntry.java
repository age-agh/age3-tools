/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model.entry;

import pl.edu.agh.age.compute.stream.problem.ProblemDefinition;

import javax.annotation.concurrent.Immutable;

@Immutable
public abstract class ProblemDefinitionEntry extends LogEntry {

	private final ProblemDefinition problemDefinition;

	private final String solutionDisplayFormat;

	ProblemDefinitionEntry(final String logEntryValues) {
		super(logEntryValues);
		problemDefinition = resolveProblemDefinition();
		solutionDisplayFormat = resolveSolutionDisplayFormat();
	}

	public ProblemDefinition problemDefinition() {
		return problemDefinition;
	}

	public String solutionDisplayFormat() {
		return solutionDisplayFormat;
	}

	public abstract BestSolutionEntry createBestSolutionEntry(final BestSolutionHeaderEntry header,
	                                                          final String logEntryValues);

	protected abstract ProblemDefinition resolveProblemDefinition();

	protected abstract String resolveSolutionDisplayFormat();
}
