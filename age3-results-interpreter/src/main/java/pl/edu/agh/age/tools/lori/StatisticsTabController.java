/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori;

import static java.util.Objects.requireNonNull;
import static javafx.collections.FXCollections.observableArrayList;

import pl.edu.agh.age.tools.lori.model.ResultModel;
import pl.edu.agh.age.tools.lori.view.SupportedCharts;

import org.gillius.jfxutils.chart.ChartPanManager;
import org.gillius.jfxutils.chart.JFXChartUtil;

import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

import javax.annotation.Nullable;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.ValueAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.util.StringConverter;

/**
 * Controller for the Statistics tab in the main window.
 */
public final class StatisticsTabController implements Initializable {

	private static final ObservableList<SupportedCharts> CHARTS = observableArrayList(SupportedCharts.values());

	private static final SupportedCharts DEFAULT_CHART = SupportedCharts.STEP_NUMBER;

	private static final ResourceBundle bundle = ResourceBundle.getBundle("pl.edu.agh.age.tools.lori.strings");

	@FXML private BorderPane statisticsView;

	@FXML private ComboBox<SupportedCharts> statisticsSelector;

	@FXML private HBox seriesContainer;

	@FXML private Label promptLabel;

	@FXML private LineChart<Number, Number> chart;

	private @Nullable ResultModel model;

	@Override public void initialize(final URL location, final ResourceBundle resources) {
		HBox.setHgrow(statisticsSelector, Priority.ALWAYS);
		statisticsSelector.setItems(CHARTS);
		statisticsSelector.getSelectionModel().select(DEFAULT_CHART);
		statisticsSelector.setConverter(new StringConverter<SupportedCharts>() {
			@Override public String toString(final SupportedCharts object) {
				return bundle.getString(object.propertiesKey());
			}

			@Override public SupportedCharts fromString(final String string) {
				throw new UnsupportedOperationException();
			}
		});
		statisticsSelector.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if ((newValue != null) && !Objects.equals(oldValue, newValue)) {
				onChartTypeChanged(newValue);
			}
		});
		setupChartAddons();
	}

	public void replaceResultModel(final ResultModel model) {
		final boolean previouslyNull = this.model == null;
		this.model = requireNonNull(model);
		clearChart(true);

		if (previouslyNull) {
			statisticsSelector.getSelectionModel().select(DEFAULT_CHART);
			statisticsView.setVisible(true);
			statisticsView.setManaged(true);
			promptLabel.setVisible(false);
			promptLabel.setManaged(false);
		}

		final SupportedCharts item = statisticsSelector.getSelectionModel().getSelectedItem();
		showChart(item.dataFromModel(model), item.yAxisPropertiesKey());
	}

	public void clearResultModel() {
		model = null;
		promptLabel.setText(bundle.getString("statistics.DEFAULT_PROMPT_TEXT"));
		statisticsView.setVisible(false);
		statisticsView.setManaged(false);
		promptLabel.setVisible(true);
		promptLabel.setManaged(true);
	}

	private void onChartTypeChanged(final SupportedCharts newType) {
		assert model != null : "Model is null when displaying the chart";
		assert newType != null : "Chart type cannot be null";

		clearChart(false);
		showChart(newType.dataFromModel(model), newType.yAxisPropertiesKey());
	}


	/**
	 * Setups the chart addons from JFXUtils library. Notice, that in order to behave properly,
	 * this method has to be invoked AFTER the chart is added to the FX node tree.
	 */
	private void setupChartAddons() {
		// Panning works via primary mouse button drags without ctrl key pressed
		final ChartPanManager panner = new ChartPanManager(chart);
		panner.setMouseFilter(event -> {
			if (!((event.getButton() == MouseButton.PRIMARY) && !event.isShortcutDown())) {
				event.consume();
			}
		});
		panner.start();

		// Zooming works only via primary mouse button without ctrl held down or secondary button drags
		JFXChartUtil.setupZooming(chart, event -> {
			if (!(((event.getButton() == MouseButton.PRIMARY) && event.isShortcutDown()) || (event.getButton()
			                                                                                 == MouseButton.SECONDARY))) {
				event.consume();
			}
		});
		JFXChartUtil.addDoublePrimaryClickAutoRangeHandler(chart);
	}


	private void clearChart(final boolean resetAxis) {
		chart.getData().clear();
		if (resetAxis) {
			chart.getYAxis().setLabel("");
			((ValueAxis<Number>)chart.getXAxis()).setLowerBound(0.0);
			((ValueAxis<Number>)chart.getYAxis()).setLowerBound(0.0);
		}
	}

	private void showChart(final Iterable<XYChart.Series<Number, Number>> seriesList, final String yAxisName) {
		seriesContainer.getChildren().clear();
		for (final XYChart.Series<Number, Number> series : seriesList) {
			chart.getData().add(series);
			chart.getYAxis().setLabel(bundle.getString(yAxisName));
			createSeriesCheckbox(series);
		}
	}

	private void createSeriesCheckbox(final XYChart.Series<Number, Number> series) {
		final CheckBox checkbox = new CheckBox(series.getName());
		checkbox.setSelected(true);
		checkbox.setOnAction(event -> {
			if (checkbox.isSelected()) {
				chart.getData().add(series);
			} else {
				// [dymara] If the chart is animated, series is removed from it after the animation finishes.
				// This could lead to throwing exceptions when adding and removing the same series quickly.
				// Temporary disabling the animation effect prevents this problem.
				chart.setAnimated(false);
				chart.getData().remove(series);
				chart.setAnimated(true);
			}
		});
		seriesContainer.getChildren().add(checkbox);
	}

}
