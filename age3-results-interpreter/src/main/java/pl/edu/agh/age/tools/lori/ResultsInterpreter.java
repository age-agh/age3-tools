/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori;

import static com.google.common.base.Preconditions.checkState;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public final class ResultsInterpreter extends Application {
	static {
		Locale.setDefault(new Locale("pl", "PL"));
	}

	private static final Logger log = LoggerFactory.getLogger(ResultsInterpreter.class);

	private static final String STYLESHEET = "stylesheet.css";

	private static final String ICON = "appicon.png";

	private static final int DEFAULT_WINDOW_WIDTH = 1280;

	private static final int DEFAULT_WINDOW_HEIGHT = 720;

	private static final int MINIMUM_WINDOW_WIDTH = 1024;

	private static final int MINIMUM_WINDOW_HEIGHT = 580;

	private static final ResourceBundle bundle = ResourceBundle.getBundle("pl.edu.agh.age.tools.lori.strings");

	private static Stage applicationStage;

	public static void main(final String... args) {
		launch(args);
	}

	@Override public void start(final Stage primaryStage) throws IOException {
		applicationStage = primaryStage;

		final FXMLLoader loader = new FXMLLoader(getClass().getResource("ri.fxml"), bundle);
		final Parent root = loader.load();
		final Scene scene = new Scene(root, DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);
		scene.getStylesheets().add(getClass().getResource(STYLESHEET).toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setTitle(bundle.getString("title"));
		primaryStage.setMinHeight(MINIMUM_WINDOW_HEIGHT);
		primaryStage.setMinWidth(MINIMUM_WINDOW_WIDTH);
		primaryStage.getIcons().add(new Image(getClass().getResource(ICON).toExternalForm()));
		primaryStage.show();

		processParameters(loader.getController(), getParameters().getRaw());

		log.info("Results interpreter started");
	}

	private void processParameters(final ResultsInterpreterController controller, final List<String> args) {
		if (!args.isEmpty()) {
			controller.lookForNewestDataFile(args.iterator().next());
		}
	}

	@SuppressWarnings("StaticVariableUsedBeforeInitialization") public static Stage stage() {
		checkState(applicationStage != null, "Application is not initialized");
		return applicationStage;
	}

	public static void resetTitle() {
		stage().setTitle(bundle.getString("title"));
	}

	public static void updateStageTitle(final String dataSetName) {
		final String applicationTitle = String.format("%s - %s", bundle.getString("title"), dataSetName);
		stage().setTitle(applicationTitle);
	}
}
