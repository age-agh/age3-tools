/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model;

import pl.edu.agh.age.compute.stream.logging.DefaultLoggingService;
import pl.edu.agh.age.tools.lori.model.entry.BestSolutionEntry;
import pl.edu.agh.age.tools.lori.model.entry.BestSolutionHeaderEntry;
import pl.edu.agh.age.tools.lori.model.entry.LabsProblemDefinitionEntry;
import pl.edu.agh.age.tools.lori.model.entry.LogEntries;
import pl.edu.agh.age.tools.lori.model.entry.LogEntryType;
import pl.edu.agh.age.tools.lori.model.entry.OgrProblemDefinitionEntry;
import pl.edu.agh.age.tools.lori.model.entry.ProblemDefinitionEntry;
import pl.edu.agh.age.tools.lori.model.entry.TickSummaryEntry;
import pl.edu.agh.age.tools.lori.model.entry.TickSummaryHeaderEntry;
import pl.edu.agh.age.tools.lori.model.entry.WorkplaceEntry;
import pl.edu.agh.age.tools.lori.model.entry.WorkplaceHeaderEntry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Extractor of {@link ResultModel} from a log file.
 */
public final class ResultModelExtractor {

	private static final Logger log = LoggerFactory.getLogger(ResultModelExtractor.class);

	public static final String LOG_FILENAME_PATTERN = "emas-\\d{8}T\\d{6}.*\\.log";

	private ResultModelExtractor() { }

	/**
	 * Obtains the result model from a given log file.
	 *
	 * @param logFile
	 * 		the log file
	 *
	 * @return the results model
	 *
	 * @throws IOException
	 * 		if the log file could not be read.
	 * @throws IllegalArgumentException
	 * 		if the log file has an incorrect format.
	 */
	public static ResultModel obtainResultModel(final File logFile) throws IOException {
		final LogEntries entries = new LogEntries();
		final ResultModelPropertiesProcessor propertiesProcessor = new ResultModelPropertiesProcessor(entries);

		try (Stream<String> stream = Files.lines(Paths.get(logFile.toURI()))) {
			stream.forEach(line -> processLine(entries, propertiesProcessor, line));
			return new ResultModel(entries, logFile.getAbsolutePath());
		} catch (final IllegalStateException e) {
			log.error("Could not build the result model", e);
			throw new IllegalArgumentException("Passed log file has incorrect format", e);
		}
	}

	private static void processLine(final LogEntries entries, final ResultModelPropertiesProcessor propertiesProcessor,
	                                final String line) {
		assert (entries != null) && (propertiesProcessor != null) && (line != null);

		final int delimiterIndex = line.indexOf(DefaultLoggingService.DELIMITER);
		if ((delimiterIndex == -1) || (delimiterIndex == (line.length() - 1))) {
			if (!propertiesProcessor.processLine(line)) {
				logUnrecognizedLine(line);
			}
			return;
		}

		final String tag = line.substring(0, delimiterIndex);
		final String values = line.substring(delimiterIndex + 1, line.length());
		final LogEntryType entryType = LogEntryType.resolveFromTag(tag);
		switch (entryType) {
			case PROBLEM_DESCRIPTION:
				processProblemDefinition(entries, values);
				break;
			case WORKPLACE_ENTRY_HEADER:
				processWorkplaceEntryHeader(entries, values);
				break;
			case WORKPLACE_ENTRY:
				processWorkplaceEntry(entries, values);
				break;
			case TICK_SUMMARY_HEADER:
				processTickSummaryHeader(entries, values);
				break;
			case TICK_SUMMARY:
				processTickSummary(entries, values);
				break;
			case BEST_SOLUTION_HEADER:
				processBestSolutionHeader(entries, values);
				break;
			case BEST_SOLUTION:
				processBestSolution(entries, values);
				break;
			case UNKNOWN:
				logUnrecognizedLine(line);
				break;
		}
	}

	private static void logUnrecognizedLine(final String line) {
		if (!line.isEmpty()) {
			log.warn("Unrecognized log entry: \"{}\"", line); // TODO line number
		}
	}

	private static void processProblemDefinition(final LogEntries entries, final String values) {
		log.debug("Processing problem definition: `{}`", values);
		final ProblemDefinitionEntry problemDefinition;
		if (values.contains("LABS")) {
			problemDefinition = new LabsProblemDefinitionEntry(values);
		} else if (values.contains("OGR")) {
			problemDefinition = new OgrProblemDefinitionEntry(values);
		} else {
			throw new IllegalArgumentException(String.format("Unrecognized problem definition in logs: `%s`", values));
		}
		entries.setProblemDefinition(problemDefinition);
	}

	private static void processWorkplaceEntryHeader(final LogEntries entries, final String values) {
		entries.setWorkplaceHeader(new WorkplaceHeaderEntry(values));
	}

	private static void processWorkplaceEntry(final LogEntries entries, final String values) {
		final WorkplaceHeaderEntry header = entries.workplaceHeader();
		try {
			final WorkplaceEntry entry = new WorkplaceEntry(header, values);
			entries.addWorkplaceEntry(entry);
		} catch (final IllegalStateException e) {
			log.error("Could not process workplace entry {}", values, e);
		}
	}

	private static void processTickSummaryHeader(final LogEntries entries, final String values) {
		entries.setTickSummaryHeader(new TickSummaryHeaderEntry(values));
	}

	private static void processTickSummary(final LogEntries entries, final String values) {
		final TickSummaryHeaderEntry header = entries.tickSummaryHeader();
		try {
			final TickSummaryEntry entry = new TickSummaryEntry(header, values);
			entries.addTickSummaryEntry(entry);
		} catch (final IllegalStateException e) {
			log.error("Could not process tick summary entry {}", values, e);
		}
	}

	private static void processBestSolutionHeader(final LogEntries entries, final String values) {
		log.debug("Processing best solution header: `{}`", values);
		entries.setBestSolutionHeader(new BestSolutionHeaderEntry(values));
	}

	private static void processBestSolution(final LogEntries entries, final String values) {
		try {
			final ProblemDefinitionEntry problemDefinition = entries.problemDefinitionEntry();
			final BestSolutionHeaderEntry header = entries.bestSolutionHeader();
			final BestSolutionEntry entry = problemDefinition.createBestSolutionEntry(header, values);
			entries.addBestSolutionEntry(entry);
		} catch (final IllegalStateException e) {
			log.error("Could not process best solution entry {}", values, e);
		}
	}

}
