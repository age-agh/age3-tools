/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model;

import static java.util.Objects.requireNonNull;

import pl.edu.agh.age.compute.stream.logging.Tags;
import pl.edu.agh.age.tools.lori.model.entry.LogEntries;

import javafx.util.Pair;

public final class ResultModelPropertiesProcessor {

	private final LogEntries entries;

	private boolean openingTagSeen = false;

	private boolean closingTagSeen = false;

	ResultModelPropertiesProcessor(final LogEntries entries) {
		this.entries = requireNonNull(entries);
	}

	/**
	 * Processes the log line with configuration properties.
	 *
	 * @param line
	 * 		the line to process
	 *
	 * @return {@code true}, if line was a valid property line, {@code false} otherwise
	 */
	public boolean processLine(final String lineToProcess) {
		requireNonNull(lineToProcess);

		if (isValidPropertiesLine(lineToProcess)) {
			if (isOpeningTag(lineToProcess)) {
				openingTagSeen = true;
				return true;
			}
			if (isClosingTag(lineToProcess)) {
				closingTagSeen = true;
				return true;
			}
			return processPropertyLine(lineToProcess);
		}
		return false;
	}

	private boolean processPropertyLine(final String lineToProcess) {
		final int delimiterIndex = lineToProcess.indexOf('=');
		if (delimiterIndex == -1) {
			entries.addProperty(new Pair<>(lineToProcess, null));
			return true;
		}
		if ((delimiterIndex > 0) && (delimiterIndex < (lineToProcess.length() - 1))) {
			final String propertyName = lineToProcess.substring(0, delimiterIndex);
			final String propertyValue = getPropertyValue(lineToProcess, delimiterIndex);
			entries.addProperty(new Pair<>(propertyName, propertyValue));
			return true;
		}
		return false;
	}

	private boolean isValidPropertiesLine(final String line) {
		if (openingTagSeen && !closingTagSeen) {
			return true;
		}
		return isOpeningTag(line);
	}

	private static String getPropertyValue(final String line, final int delimiterIndex) {
		final String rawPropertyValue = line.substring(delimiterIndex + 1, line.length());
		if (rawPropertyValue.startsWith("pl.edu.agh")) {
			final int lastDotIndex = rawPropertyValue.lastIndexOf('.');
			return rawPropertyValue.substring(lastDotIndex + 1, rawPropertyValue.length());
		}
		return rawPropertyValue;
	}

	private static boolean isOpeningTag(final String line) {
		return line.startsWith(Tags.PROPERTIES_OPENING_TAG);
	}

	private static boolean isClosingTag(final String line) {
		return line.startsWith(Tags.PROPERTIES_CLOSING_TAG);
	}

}
