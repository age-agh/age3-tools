/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.model.entry;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.requireNonNull;

import pl.edu.agh.age.compute.stream.emas.StatisticsKeys;
import pl.edu.agh.age.compute.stream.logging.Columns;

import javax.annotation.concurrent.Immutable;

/**
 * An entry describing statistics in a single workplace.
 */
@Immutable
public final class WorkplaceEntry extends LogEntry {

	private final double time;

	private final long workplaceId;

	private final long stepNumber;

	private final long populationSize;

	private final double energySum;

	private final double averageFitness;

	public WorkplaceEntry(final WorkplaceHeaderEntry header, final String logEntryValues) {
		super(logEntryValues);
		requireNonNull(header);
		checkState(header.length() <= length());

		time = getTimeAsDouble(header.indexOf(Columns.TIME));
		workplaceId = getAsLong(header.indexOf(Columns.WORKPLACE_ID));
		stepNumber = getAsLong(header.indexOf(StatisticsKeys.STEP_NUMBER.toString()));
		populationSize = getAsLong(header.indexOf(StatisticsKeys.POPULATION_SIZE.toString()));
		energySum = getAsDouble(header.indexOf(StatisticsKeys.ENERGY_SUM.toString()));
		averageFitness = getAsDouble(header.indexOf(StatisticsKeys.AVERAGE_FITNESS.toString()));
	}

	public double time() {
		return time;
	}

	public long workplaceId() {
		return workplaceId;
	}

	public long stepNumber() {
		return stepNumber;
	}

	public long populationSize() {
		return populationSize;
	}

	public double populationEnergySum() {
		return energySum;
	}

	/**
	 * @return average energy size of population and 0 if population is empty.
	 */
	public double averageEnergy() {
		return (populationSize == 0) ? 0 : (energySum / populationSize);
	}

	public double averageFitness() {
		return averageFitness;
	}

}
