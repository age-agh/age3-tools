/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.tools.lori.view;

import pl.edu.agh.age.tools.lori.model.ResultModel;

import one.util.streamex.StreamEx;

import java.util.List;

import io.vavr.Function1;
import javafx.scene.chart.XYChart;

@SuppressWarnings("ImmutableEnumChecker")
public enum SupportedCharts {
	STEP_NUMBER("STEP_NUMBER", ResultModel::stepNumberChartData),
	BEST_FITNESS("BEST_FITNESS", ResultModel::bestFitnessChartData),
	AVERAGE_FITNESS("AVERAGE_FITNESS", ResultModel::averageFitnessChartData),
	POPULATION_SIZE("POPULATION_SIZE", ResultModel::populationSizeChartData),
	ENERGY_SUM("ENERGY_SUM", ResultModel::energySumChartData),
	AVERAGE_ENERGY("AVERAGE_ENERGY", ResultModel::averageEnergyChartData),
	FITNESS_EVALUATIONS("FITNESS_EVALUATIONS", ResultModel::fitnessCountChartData);

	private final String chart;

	private final Function1<ResultModel, Iterable<XYChart.Series<Number, Number>>> seriesGetter;

	SupportedCharts(final String chart,
	                final Function1<ResultModel, Iterable<XYChart.Series<Number, Number>>> seriesGetter) {
		this.chart = chart;
		this.seriesGetter = seriesGetter;
	}

	public String chartName() {
		return chart + "_CHART";
	}

	public String yAxisName() {
		return chart + "_Y_AXIS_NAME";
	}

	public String propertiesKey() {
		return "statistics." + chartName();
	}

	public String yAxisPropertiesKey() {
		return "statistics." + yAxisName();
	}

	public static List<String> allPropertyKeys() {
		return StreamEx.of(values()).map(SupportedCharts::propertiesKey).toList();
	}

	public Iterable<XYChart.Series<Number, Number>> dataFromModel(final ResultModel model) {
		return seriesGetter.apply(model);
	}
}
