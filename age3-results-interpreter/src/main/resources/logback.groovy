/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */


import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.classic.filter.ThresholdFilter

def bySecond = timestamp("yyyyMMdd'T'HHmmss")

appender("FILE", FileAppender) {
	file = "ri-${bySecond}.log"
	append = false
	filter(ThresholdFilter) {
		level = DEBUG
	}
	encoder(PatternLayoutEncoder) {
		pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{40} - %msg%n"
	}
}

appender("CONSOLE", ConsoleAppender) {
	filter(ThresholdFilter) {
		level = DEBUG
	}
	encoder(PatternLayoutEncoder) {
		pattern = "%highlight(%.-1level) %green(%-40logger{39}) : %msg%n"
	}
}

root(ALL, ["FILE"])
logger("pl.edu.agh.age", DEBUG, ["CONSOLE"])

