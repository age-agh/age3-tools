# AgE 3 Tools

This repository contains various tools for [AgE 3](https://www.age.agh.edu.pl/).

**This is beta software** and is still under heavy development.
Although this version is released, it is only a *snapshot* of a current development branch and
there is no guarantee that future version will provide the same functionality or API.

Currently the following tools are available:

- LABS-OGR Results Interpreter (currently at version 0.1).

## LABS-OGR Results Interpreter

### Buildling standalone executable jar

To build the standalone executable jar file execute:

```bash
./gradlew age3-results-interpreter:shadowJar
```

The jar file will be built as: `age3-results-interpreter/build/libs/age3-lori-VERSION.jar`.
Where `version` is the current version of the module.

To run it, execute:

```bash
java -jar age3-results-interpreter/build/libs/age3-lori-VERSION.jar
```

### Requirements

LABS-OGR Results Interpreter requires JDK with JavaFX at version 8 or higher.

## More documentation

For more documentation see the `docs/` directory in the main repo or check the HTML version on <https://docs.age.agh.edu.pl/age3/>.

## Links

* [Repository on GitLab](https://gitlab.com/age-agh/age3-tools)
* [Maven Repository](https://repo.age.agh.edu.pl/repository/maven-public/)
* [Documentation](https://docs.age.agh.edu.pl/age3/)
* [Javadocs](https://docs.age.agh.edu.pl/javadocs/age3-tools/develop/)
