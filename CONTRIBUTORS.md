Contributors are ordered by the first contribution.

- [Kamil Piętak](https://gitlab.com/kpietak) <kpietak@agh.edu.pl>
- [Łukasz Faber](https://gitlab.com/Nnidyu) <faber@agh.edu.pl>
- [Andrzej Dymara](https://gitlab.com/dymara) <andrzejdymara@gmail.com>
